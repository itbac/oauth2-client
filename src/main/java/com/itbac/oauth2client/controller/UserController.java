package com.itbac.oauth2client.controller;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: BacHe
 * @email: 1218585258@qq.com
 * @Date: 2021/3/27 22:16
 */
@RestController
@RequestMapping("/user")
public class UserController {

//http://localhost:8080/user/userInfo

    @RequestMapping("/userInfo")
    public Object userInfo(Authentication authentication){
        return authentication;
    }
}
